webpackJsonp([5],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiseAlarmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
* Generated class for the RiseAlarmPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
var RiseAlarmPage = /** @class */ (function () {
    function RiseAlarmPage(navCtrl, navParams, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.eventSource = [];
        this.selectedDay = new Date();
        this.calendar = {
            mode: 'month',
            currentDate: new Date()
        };
    }
    RiseAlarmPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiseAlarmPage');
    };
    RiseAlarmPage.prototype.addEvent = function () {
        var _this = this;
        var modal = this.modalCtrl.create('EventModalPage', { selectedDay: this.selectedDay });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (data) {
                var eventData = data;
                eventData.startTime = new Date(data.startTime);
                eventData.endTime = new Date(data.endTime);
                var events_1 = _this.eventSource;
                events_1.push(eventData);
                _this.eventSource = [];
                setTimeout(function () {
                    _this.eventSource = events_1;
                });
            }
        });
    };
    RiseAlarmPage.prototype.onViewTitleChanged = function (title) {
        this.viewTitle = title;
    };
    RiseAlarmPage.prototype.onEventSelected = function (event) {
        var start = __WEBPACK_IMPORTED_MODULE_2_moment__(event.startTime).format('LLLL');
        var end = __WEBPACK_IMPORTED_MODULE_2_moment__(event.endTime).format('LLLL');
        var alert = this.alertCtrl.create({
            title: '' + event.title,
            subTitle: 'From: ' + start + '<br>To: ' + end,
            buttons: ['OK']
        });
        alert.present();
    };
    RiseAlarmPage.prototype.onTimeSelected = function (ev) {
        this.selectedDay = ev.selectedTime;
    };
    RiseAlarmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-rise-alarm',template:/*ion-inline-start:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-alarm\rise-alarm.html"*/'<ion-header no-border>\n\n  <ion-navbar>\n\n    <ion-title>Alarm</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content fullscreen>\n\n  <ion-item-group>\n\n    <ion-item-divider class="list-item-style" color="secondary">Alarm Schedule</ion-item-divider>\n\n  </ion-item-group>\n\n\n\n  <ion-grid class="menu-margin font-normal">\n\n    <ion-row justify-content-center>\n\n      <ion-label class="font-bold">Alarm Days</ion-label>\n\n    </ion-row>\n\n    <ion-buttons>\n\n      <button ion-button block (click)="addEvent()">\n\n        <ion-icon name="add" color="secondary"></ion-icon>\n\n        Add Event\n\n      </button>\n\n    </ion-buttons>\n\n\n\n    <calendar [eventSource]="eventSource"\n\n    [calendarMode]="calendar.mode"\n\n    [currentDate]="calendar.currentDate"\n\n    (onEventSelected)="onEventSelected($event)"\n\n    (onTitleChanged)="onViewTitleChanged($event)"\n\n    (onTimeSelected)="onTimeSelected($event)"\n\n    step="30"\n\n    class="calendar">\n\n  </calendar>\n\n</ion-grid>\n\n\n\n<ion-item-group>\n\n        <ion-item-divider class="list-item-style" color="secondary">Alarm Settings</ion-item-divider>\n\n    <ion-item no-lines>\n\n        <ion-label class="font-regular">Wake-Up window</ion-label>\n\n        <ion-select [(ngModel)]="val_perc">\n\n            <ion-option value="val_30">30 min</ion-option>\n\n            <ion-option value="val_25">25 min</ion-option>\n\n            <ion-option value="val_20">20 min</ion-option>\n\n            <ion-option value="val_15">15 min</ion-option>\n\n            <ion-option value="val_10">10 min</ion-option>\n\n        </ion-select>\n\n    </ion-item>\n\n    <ion-item no-lines>\n\n        <ion-label class="font-regular">Use Tracker</ion-label>\n\n        <ion-toggle value="use_tracker" checked="true"></ion-toggle>\n\n    </ion-item>\n\n    <ion-item no-lines>\n\n        <ion-label class="font-regular">Use HomeKit</ion-label>\n\n        <ion-toggle value="use_homekit" checked="true"></ion-toggle>\n\n    </ion-item>\n\n</ion-item-group>\n\n<ion-buttons>\n\n  <button ion-button full (click)="addEvent()">\n\n    <ion-icon name="save" color="secondary"></ion-icon>\n\n    Save Changes\n\n  </button>\n\n</ion-buttons>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-alarm\rise-alarm.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
    ], RiseAlarmPage);
    return RiseAlarmPage;
}());

//# sourceMappingURL=rise-alarm.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiseLightPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RiseLightPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RiseLightPage = /** @class */ (function () {
    function RiseLightPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RiseLightPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiseLightPage');
    };
    RiseLightPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-rise-light',template:/*ion-inline-start:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-light\rise-light.html"*/'<ion-header no-border>\n\n\n\n    <ion-navbar>\n\n        <ion-title>RISE</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content fullscreen>\n\n    <ion-item-group>\n\n        <ion-item-divider class="list-item-style" color="secondary">Color Settings</ion-item-divider>\n\n        <ion-item-divider class="list-item-style" color="white">Color Gradient</ion-item-divider>\n\n    </ion-item-group>\n\n\n\n    <ion-grid class="menu-margin font-normal">\n\n        <ion-row justify-content-center>\n\n            <input type="color" value="#ffffff" class="gradient-button">\n\n            <div class="gradient"></div>\n\n            <input type="color" value="#e73c11" class="gradient-button">\n\n        </ion-row>\n\n    </ion-grid>\n\n    <ion-item no-lines>\n\n        <ion-label class="font-regular">Use "Time-of-Day" Colors</ion-label>\n\n        <ion-toggle value="time_of_day" checked="true"></ion-toggle>\n\n    </ion-item>\n\n    <ion-item-group>\n\n            <ion-item-divider class="list-item-style" color="secondary">Other Settings</ion-item-divider>\n\n        <ion-item no-lines>\n\n            <ion-label class="font-regular">Maximal Brightness</ion-label>\n\n            <ion-select [(ngModel)]="val_perc">\n\n                <ion-option value="val_100">100%</ion-option>\n\n                <ion-option value="val_80">80%</ion-option>\n\n                <ion-option value="val_60">60%</ion-option>\n\n                <ion-option value="val_40">40%</ion-option>\n\n                <ion-option value="val_20">20%</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item no-lines>\n\n            <ion-label class="font-regular">Fade Speed</ion-label>\n\n            <ion-select [(ngModel)]="val_sec">\n\n                <ion-option value="val_15">15s</ion-option>\n\n                <ion-option value="val_10">10s</ion-option>\n\n                <ion-option value="val_5">5s</ion-option>\n\n                <ion-option value="val_3">3s</ion-option>\n\n                <ion-option value="val_1">1s</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n    </ion-item-group>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-light\rise-light.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], RiseLightPage);
    return RiseLightPage;
}());

//# sourceMappingURL=rise-light.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiseMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rise_light_rise_light__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__rise_alarm_rise_alarm__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RiseMenuPage = /** @class */ (function () {
    function RiseMenuPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RiseMenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiseMenuPage');
    };
    RiseMenuPage.prototype.openLight = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__rise_light_rise_light__["a" /* RiseLightPage */]);
    };
    RiseMenuPage.prototype.openAlarm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__rise_alarm_rise_alarm__["a" /* RiseAlarmPage */]);
    };
    RiseMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-rise-menu',template:/*ion-inline-start:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-menu\rise-menu.html"*/'<ion-header no-border>\n\n  <ion-navbar>\n\n    <ion-title>RISE</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content fullscreen>\n\n  <ion-grid class="menu-margin font-normal">\n\n    <ion-row justify-content-center class="row-parent">\n\n      <ion-col col-auto class="menu-item" (click)="openAlarm()">\n\n        <ion-icon name="notifications-outline"></ion-icon>\n\n        <ion-label>Alarm</ion-label>\n\n        <p>Set & customize Alarms</p>\n\n      </ion-col>\n\n      <ion-col col-auto class="menu-item" (click)="openLight()">\n\n        <ion-icon name="bulb"></ion-icon>\n\n        <ion-label>Light</ion-label>\n\n        <p>Control the light</p>\n\n      </ion-col>\n\n      <ion-col col-auto class="menu-item inactive">\n\n        <ion-icon name="volume-up" class="inactive"></ion-icon>\n\n        <ion-label class="inactive">Sound</ion-label>\n\n        <p>Disabled</p>\n\n      </ion-col>\n\n      <ion-col col-auto class="menu-item inactive">\n\n        <ion-icon name="apps" class="inactive"></ion-icon>\n\n        <ion-label class="inactive">Buttons</ion-label>\n\n        <p>Disabled</p>\n\n      </ion-col>\n\n      <ion-col col-auto class="menu-item inactive">\n\n        <ion-icon name="pulse" class="inactive"></ion-icon>\n\n        <ion-label class="inactive">Sleep Tracker</ion-label>\n\n        <p>Disabled</p>\n\n      </ion-col>\n\n      <ion-col col-auto class="menu-item white">\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-menu\rise-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], RiseMenuPage);
    return RiseMenuPage;
}());

//# sourceMappingURL=rise-menu.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiseStartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rise_menu_rise_menu__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RiseStartPage = /** @class */ (function () {
    function RiseStartPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RiseStartPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiseStartPage');
    };
    RiseStartPage.prototype.openMenuScreen = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__rise_menu_rise_menu__["a" /* RiseMenuPage */], {}, { animate: true, direction: 'forward' });
        console.log("Triggered openMenuScree()");
    };
    RiseStartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-rise-start',template:/*ion-inline-start:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-start\rise-start.html"*/'\n\n<ion-content fullscreen>\n\n  <div  class="icon-center" (click)="openMenuScreen()">\n\n    <img src="assets/imgs/rise_icon.png">\n\n    <ion-label class="font-label icon-label">RISE</ion-label>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\pages\rise-start\rise-start.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], RiseStartPage);
    return RiseStartPage;
}());

//# sourceMappingURL=rise-start.js.map

/***/ }),

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/event-modal/event-modal.module": [
		410,
		0
	],
	"../pages/rise-alarm/rise-alarm.module": [
		411,
		4
	],
	"../pages/rise-light/rise-light.module": [
		412,
		3
	],
	"../pages/rise-menu/rise-menu.module": [
		413,
		2
	],
	"../pages/rise-start/rise-start.module": [
		414,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 155;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(345);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_rise_start_rise_start__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_rise_menu_rise_menu__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_rise_alarm_rise_alarm__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_rise_light_rise_light__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ionic2_calendar__ = __webpack_require__(399);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_rise_start_rise_start__["a" /* RiseStartPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_rise_menu_rise_menu__["a" /* RiseMenuPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_rise_alarm_rise_alarm__["a" /* RiseAlarmPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_rise_light_rise_light__["a" /* RiseLightPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/event-modal/event-modal.module#EventModalPageModule', name: 'EventModalPage', segment: 'event-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rise-alarm/rise-alarm.module#RiseAlarmPageModule', name: 'RiseAlarmPage', segment: 'rise-alarm', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rise-light/rise-light.module#RiseLightPageModule', name: 'RiseLightPage', segment: 'rise-light', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rise-menu/rise-menu.module#RiseMenuPageModule', name: 'RiseMenuPage', segment: 'rise-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rise-start/rise-start.module#RiseStartPageModule', name: 'RiseStartPage', segment: 'rise-start', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_10_ionic2_calendar__["a" /* NgCalendarModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_rise_start_rise_start__["a" /* RiseStartPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_rise_menu_rise_menu__["a" /* RiseMenuPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_rise_alarm_rise_alarm__["a" /* RiseAlarmPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_rise_light_rise_light__["a" /* RiseLightPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 156,
	"./af.js": 156,
	"./ar": 157,
	"./ar-dz": 158,
	"./ar-dz.js": 158,
	"./ar-kw": 159,
	"./ar-kw.js": 159,
	"./ar-ly": 160,
	"./ar-ly.js": 160,
	"./ar-ma": 161,
	"./ar-ma.js": 161,
	"./ar-sa": 162,
	"./ar-sa.js": 162,
	"./ar-tn": 163,
	"./ar-tn.js": 163,
	"./ar.js": 157,
	"./az": 164,
	"./az.js": 164,
	"./be": 165,
	"./be.js": 165,
	"./bg": 166,
	"./bg.js": 166,
	"./bm": 167,
	"./bm.js": 167,
	"./bn": 168,
	"./bn.js": 168,
	"./bo": 169,
	"./bo.js": 169,
	"./br": 170,
	"./br.js": 170,
	"./bs": 171,
	"./bs.js": 171,
	"./ca": 172,
	"./ca.js": 172,
	"./cs": 173,
	"./cs.js": 173,
	"./cv": 174,
	"./cv.js": 174,
	"./cy": 175,
	"./cy.js": 175,
	"./da": 176,
	"./da.js": 176,
	"./de": 177,
	"./de-at": 178,
	"./de-at.js": 178,
	"./de-ch": 179,
	"./de-ch.js": 179,
	"./de.js": 177,
	"./dv": 180,
	"./dv.js": 180,
	"./el": 181,
	"./el.js": 181,
	"./en-au": 182,
	"./en-au.js": 182,
	"./en-ca": 183,
	"./en-ca.js": 183,
	"./en-gb": 184,
	"./en-gb.js": 184,
	"./en-ie": 185,
	"./en-ie.js": 185,
	"./en-il": 186,
	"./en-il.js": 186,
	"./en-nz": 187,
	"./en-nz.js": 187,
	"./eo": 188,
	"./eo.js": 188,
	"./es": 189,
	"./es-do": 190,
	"./es-do.js": 190,
	"./es-us": 191,
	"./es-us.js": 191,
	"./es.js": 189,
	"./et": 192,
	"./et.js": 192,
	"./eu": 193,
	"./eu.js": 193,
	"./fa": 194,
	"./fa.js": 194,
	"./fi": 195,
	"./fi.js": 195,
	"./fo": 196,
	"./fo.js": 196,
	"./fr": 197,
	"./fr-ca": 198,
	"./fr-ca.js": 198,
	"./fr-ch": 199,
	"./fr-ch.js": 199,
	"./fr.js": 197,
	"./fy": 200,
	"./fy.js": 200,
	"./gd": 201,
	"./gd.js": 201,
	"./gl": 202,
	"./gl.js": 202,
	"./gom-latn": 203,
	"./gom-latn.js": 203,
	"./gu": 204,
	"./gu.js": 204,
	"./he": 205,
	"./he.js": 205,
	"./hi": 206,
	"./hi.js": 206,
	"./hr": 207,
	"./hr.js": 207,
	"./hu": 208,
	"./hu.js": 208,
	"./hy-am": 209,
	"./hy-am.js": 209,
	"./id": 210,
	"./id.js": 210,
	"./is": 211,
	"./is.js": 211,
	"./it": 212,
	"./it.js": 212,
	"./ja": 213,
	"./ja.js": 213,
	"./jv": 214,
	"./jv.js": 214,
	"./ka": 215,
	"./ka.js": 215,
	"./kk": 216,
	"./kk.js": 216,
	"./km": 217,
	"./km.js": 217,
	"./kn": 218,
	"./kn.js": 218,
	"./ko": 219,
	"./ko.js": 219,
	"./ky": 220,
	"./ky.js": 220,
	"./lb": 221,
	"./lb.js": 221,
	"./lo": 222,
	"./lo.js": 222,
	"./lt": 223,
	"./lt.js": 223,
	"./lv": 224,
	"./lv.js": 224,
	"./me": 225,
	"./me.js": 225,
	"./mi": 226,
	"./mi.js": 226,
	"./mk": 227,
	"./mk.js": 227,
	"./ml": 228,
	"./ml.js": 228,
	"./mn": 229,
	"./mn.js": 229,
	"./mr": 230,
	"./mr.js": 230,
	"./ms": 231,
	"./ms-my": 232,
	"./ms-my.js": 232,
	"./ms.js": 231,
	"./mt": 233,
	"./mt.js": 233,
	"./my": 234,
	"./my.js": 234,
	"./nb": 235,
	"./nb.js": 235,
	"./ne": 236,
	"./ne.js": 236,
	"./nl": 237,
	"./nl-be": 238,
	"./nl-be.js": 238,
	"./nl.js": 237,
	"./nn": 239,
	"./nn.js": 239,
	"./pa-in": 240,
	"./pa-in.js": 240,
	"./pl": 241,
	"./pl.js": 241,
	"./pt": 242,
	"./pt-br": 243,
	"./pt-br.js": 243,
	"./pt.js": 242,
	"./ro": 244,
	"./ro.js": 244,
	"./ru": 245,
	"./ru.js": 245,
	"./sd": 246,
	"./sd.js": 246,
	"./se": 247,
	"./se.js": 247,
	"./si": 248,
	"./si.js": 248,
	"./sk": 249,
	"./sk.js": 249,
	"./sl": 250,
	"./sl.js": 250,
	"./sq": 251,
	"./sq.js": 251,
	"./sr": 252,
	"./sr-cyrl": 253,
	"./sr-cyrl.js": 253,
	"./sr.js": 252,
	"./ss": 254,
	"./ss.js": 254,
	"./sv": 255,
	"./sv.js": 255,
	"./sw": 256,
	"./sw.js": 256,
	"./ta": 257,
	"./ta.js": 257,
	"./te": 258,
	"./te.js": 258,
	"./tet": 259,
	"./tet.js": 259,
	"./tg": 260,
	"./tg.js": 260,
	"./th": 261,
	"./th.js": 261,
	"./tl-ph": 262,
	"./tl-ph.js": 262,
	"./tlh": 263,
	"./tlh.js": 263,
	"./tr": 264,
	"./tr.js": 264,
	"./tzl": 265,
	"./tzl.js": 265,
	"./tzm": 266,
	"./tzm-latn": 267,
	"./tzm-latn.js": 267,
	"./tzm.js": 266,
	"./ug-cn": 268,
	"./ug-cn.js": 268,
	"./uk": 269,
	"./uk.js": 269,
	"./ur": 270,
	"./ur.js": 270,
	"./uz": 271,
	"./uz-latn": 272,
	"./uz-latn.js": 272,
	"./uz.js": 271,
	"./vi": 273,
	"./vi.js": 273,
	"./x-pseudo": 274,
	"./x-pseudo.js": 274,
	"./yo": 275,
	"./yo.js": 275,
	"./zh-cn": 276,
	"./zh-cn.js": 276,
	"./zh-hk": 277,
	"./zh-hk.js": 277,
	"./zh-tw": 278,
	"./zh-tw.js": 278
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 372;

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_rise_start_rise_start__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(321);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, menu, statusBar, splashScreen) {
        this.platform = platform;
        this.menu = menu;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        // make HelloIonicPage the root (or first) page
        //pages: Array<{title: string, component: any}>;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__pages_rise_start_rise_start__["a" /* RiseStartPage */];
        this.initializeApp();
        // set our app's pages
        /*this.pages = [
          { title: 'Hello Ionic', component: HelloIonicPage },
          { title: 'My First List', component: ListPage }
        ]; */
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // navigate to the new page if it is not the current page
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>'/*ion-inline-end:"C:\Users\K.Taro\Documents\Workspaces\Ionic\rise\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 403:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[322]);
//# sourceMappingURL=main.js.map