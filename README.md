## Prototype
https://xd.adobe.com/view/6bac39a3-47d0-4de7-4821-b7c28a6eb6e5-1d9d/

## Set up the Project


1. Install Node/NPM

    https://nodejs.org/en/download/

2. Install Ionic and (optionally) Cordova

    ```npm install -g ionic cordova```

3. Clone Repository to local git

    ```git clone https://gitlab.com/Brieger/rise.git```

4. Install Web-Dev IDE of Choice

5. Navigate to local folder and install dependencies

    ```npm install```

6. Start server

    ```ionic serve```