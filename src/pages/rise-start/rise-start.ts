import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RiseMenuPage } from '../rise-menu/rise-menu';


@IonicPage()
@Component({
  selector: 'page-rise-start',
  templateUrl: 'rise-start.html',
})
export class RiseStartPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiseStartPage');
  }

  openMenuScreen() {
    this.navCtrl.setRoot(RiseMenuPage, {}, {animate: true, direction: 'forward'});
    console.log("Triggered openMenuScree()");
  }

}
