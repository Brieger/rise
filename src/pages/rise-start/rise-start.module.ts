import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RiseStartPage } from './rise-start';

@NgModule({
  declarations: [
    RiseStartPage,
  ],
  imports: [
    IonicPageModule.forChild(RiseStartPage),
  ],
})
export class RiseStartPageModule {}
