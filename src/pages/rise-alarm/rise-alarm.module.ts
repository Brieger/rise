import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RiseAlarmPage } from './rise-alarm';

@NgModule({
  declarations: [
    RiseAlarmPage,
  ],
  imports: [
    IonicPageModule.forChild(RiseAlarmPage),
  ],
})
export class RiseAlarmPageModule {}
