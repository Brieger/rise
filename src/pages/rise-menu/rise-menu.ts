import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RiseLightPage } from '../rise-light/rise-light';
import { RiseAlarmPage } from '../rise-alarm/rise-alarm';

@IonicPage()
@Component({
  selector: 'page-rise-menu',
  templateUrl: 'rise-menu.html',
})
export class RiseMenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiseMenuPage');
  }

  openLight() {
    this.navCtrl.push(RiseLightPage);
  }

  openAlarm() {
    this.navCtrl.push(RiseAlarmPage);
  }

}
