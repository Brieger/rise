import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RiseMenuPage } from './rise-menu';

@NgModule({
  declarations: [
    RiseMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(RiseMenuPage),
  ],
})
export class RiseMenuPageModule {}
