import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RiseLightPage } from './rise-light';

@NgModule({
  declarations: [
    RiseLightPage,
  ],
  imports: [
    IonicPageModule.forChild(RiseLightPage),
  ],
})
export class RiseLightPageModule {}
