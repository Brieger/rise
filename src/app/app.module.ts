import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { RiseStartPage } from '../pages/rise-start/rise-start';
import { RiseMenuPage } from '../pages/rise-menu/rise-menu';
import { RiseAlarmPage } from '../pages/rise-alarm/rise-alarm';
import { RiseLightPage } from '../pages/rise-light/rise-light';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NgCalendarModule  } from 'ionic2-calendar';




@NgModule({
  declarations: [
    MyApp,
    RiseStartPage,
    RiseMenuPage,
    RiseAlarmPage,
    RiseLightPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgCalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RiseStartPage,
    RiseMenuPage,
    RiseAlarmPage,
    RiseLightPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
